#!/bin/bash

declare -r wdir="$HOME/environments"
declare -r script_path="$(realpath $0)"
declare -r script_dir="$(dirname $script_path)"
declare -r repo_dir="${script_dir%/*}"
declare -r cfg_file="$repo_dir/ansible/ansible.cfg"
declare -r inv_file="$repo_dir/ansible/hosts"

function setup_ansible_env {
    mkdir -p $wdir
    cd $wdir
    easy_install --user pip
    $HOME/.local/bin/pip install --user --upgrade pip
    $HOME/.local/bin/pip install --user virtualenv
    $HOME/.local/bin/virtualenv ansible
    source $wdir/ansible/bin/activate
    $wdir/ansible/bin/pip install ansible
}

case "$1" in
    -ni | --no-install )
	declare -r role=$2
	declare -i install=1
	;;
    * )
	declare -r role=$1
	declare -i install=0
	;;
esac

declare -r roles="$(ls $repo_dir/ansible/roles/)"
if [[ "$roles" != *"$role"* ]]; then
    echo -e "\nUnrecognized role. Choose from:\n$roles\n"
    exit 1
fi

if [[ $install == 0 ]]; then
    setup_ansible_env
fi

source $wdir/ansible/bin/activate

# Run ansible
export ANSIBLE_CONFIG="$cfg_file"
ansible-playbook \
    --ask-become-pass \
    --inventory-file="$inv_file" \
    --extra-vars "role=$role repo_dir=$repo_dir venv_dir=$wdir/ansible cfg_file=$cfg_file inv_file=$inv_file" \
    $repo_dir/ansible/site.yml \

